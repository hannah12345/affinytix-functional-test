#!/bin/bash
HOST1="widget.wepingo.com"
HOST2="api-aff.wepingo.com"
HOST3="api-convertor.wepingo.com"
sed -i "/$HOST1/ s/.*/$1\t$HOST1/g" /etc/hosts
sed -i "/$HOST2/ s/.*/$1\t$HOST2/g" /etc/hosts
sed -i "/$HOST3/ s/.*/$1\t$HOST3/g" /etc/hosts
